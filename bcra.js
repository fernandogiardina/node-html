var cheerio = require('cheerio')
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: false })
var columnify = require('columnify');

var url = "https://www.bcra.gob.ar/BCRAyVos/Situacion_Crediticia.asp"

run(url)


function run(url) {
    nightmare
    .goto(url)
    .wait('.g-recaptcha')
    .evaluate(function () {
        return document.getElementsByTagName("FORM")[7].innerHTML;
    })
    .end()
    .then(function (result) {        
        console.log(result)
    })
    .catch(function (error) {
        console.log(JSON.stringify({ 'error' : error }))
    });

}

var parse = function(html) {
    
    const $ = cheerio.load(html)

    $('.g-recaptcha').find('.soccer').each(function(i, elem) {
    
    });


    var matches = []
    var matchesIndex = 0
    
    var todayArray = $('.today').text().split(" ")[0].split("/")
    var month = todayArray[0]
    var day = todayArray[1]

    $('#fs').find('.soccer').each(function(i, elem) {

        
        pais = $(this).find('.country_part').text()
        liga = $(this).find('.tournament_part').text()

        $(this).find('tbody').find('tr').each(function(i, elem) {

            isLive = $(this).hasClass('stage-live')
            isSchedule = $(this).hasClass('stage-scheduled')
            isFinished = $(this).hasClass('stage-finished')

            playing = $(this).find('.playing').find('span').text()
            isBreak = (playing.length > 5) ? true : false

            local = $(this).find('.padr').text().trim()
            visitante = $(this).find('.padl').text().trim()

            if ($(this).hasClass('match-comments')) {
                localScore = 0
                visitanteScore = 0
            }
            else {
                resultado = $(this).find('.cell_sa').text().trim().split('-')
                if (resultado.length == 2) {
                    localScore = resultado[0].trim()
                    visitanteScore = resultado[1].trim()
                }
                else {
                    localScore = "0"
                    visitanteScore = "0"
                }
            }
            

            time = $(this).find('.time').text().trim()

            currentTime = isBreak ? 0 : (isLive ? $(this).find('.timer').text().trim() : 0)
            var state = (isBreak ? 'Break' : isLive ? "In process" : (isFinished ? "Finished" : "Pending"))

            var d = new Date()
            matches[matchesIndex] =  {
                country : pais.trim(),
                league : liga.trim(),
                day: month + " " + day,
                schedule : time,
                state : state,
                time : currentTime,
                club_local : local,
                score_local : localScore,
                club_visitor : visitante,
                score_visitor : visitanteScore
            }

            matchesIndex++

        });
        
    });

    //return (JSON.stringify({ 'datetime': new Date(), 'source': url , 'items' : resultArr }))
    return formatter(matches);
};

var formatter = function(data) {

    var columns = columnify(data, {
        columns: ['country','league','day', 'schedule', 'state', 'time','club_local','score_local','club_visitor','score_visitor']
      });

    return columns

};