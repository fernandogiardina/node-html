var cheerio = require('cheerio');
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: false });
var columnify = require('columnify');

var url = "https://www.resultados.com/es/futbol/argentina/superliga/";

run(url);

function run(url) {
    nightmare
    .goto(url)
    .wait('#table-type-1')
    .evaluate(function () {
        return document.getElementById('box-table-type-1').innerHTML;
    })
    .end()
    .then(function (result) {        
        console.log(parse(result));
    })
    .catch(function (error) {
        console.log(JSON.stringify({ 'error' : error }));
    });

}

var parse = function(html) {
    
    var data = [];
    
    const $ = cheerio.load(html)
    $('table').find('tbody').find('tr').each(function(i, elem) {

        data[i] = {
            rank: $(elem).find('.rank').text(),
            team_name: $(elem).find('.team_name_span').text(),
            matches_played: $(elem).find('.matches_played').text(),
            wins: $(elem).find('.wins').text(),
            draws: $(elem).find('.draws').text(),
            losses: $(elem).find('.losses').text(),
            points: $(elem).find('.goals').next().text()
        };

    });

    return formatter(data)

};

var formatter = function(data) {

    var columns = columnify(data, {
        columns: ['rank', 'team_name','matches_played','wins','draws','losses','points']
      });

    return columns

};