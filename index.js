const request = require("request")
const cheerio = require('cheerio')

var express = require('express');
var app = express();

app.get('/resultados', function (req, res) {
    request("https://www.resultados.com/es/futbol/argentina/superliga/resultados/", function (error, response, body) {
        if (!error) {
            res.setHeader('Content-Type', 'application/json')
            res.send(parse(body))
        } else {
            console.log(error)
        }
    });

  
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

var parse = function(html) {
    
    
    const $ = cheerio.load(html)

    let resultados = $('#tournament-page-data-results').text().trim()
    var partidos = resultados.split('¬AB÷3¬CR÷3¬AC÷3¬CX÷');

    console.log("")

    var ultimaJornada = ""
    var resultArr = []
    for (var i in partidos.slice(0, 20)) {
        
        if (i == 0) { continue; }
        
        var partida = partidos[i]

        // Jornada
        var jornadaarr = partida.split('÷')
        var jornada = jornadaarr[1].split('¬')[0]
        
        // Equipo 1
        var equipo1 = partida.split('¬WU÷').pop().split('¬AG÷').shift().split('¬')[0].replace(/-/g,' ') 

        // Equipo 2
        var equipo2 = partida.split('¬WV÷').pop().split('¬').shift().split('¬')[0].replace(/-/g,' ')

        // resultado1
        var resultado1 = partida.split('AG÷').pop().split('¬').shift()[0]

        // resultado2
        var resultado2 = partida.split('AH÷').pop().split('¬').shift()[0]

        // Resultados
        // console.log(jornada)
        // console.log(equipo1 + ": " + resultado1)
        // console.log(equipo2 + ": " + resultado2)
        // console.log("")



        var jsonData =  {
                'jornada' : jornada,
                'local': {
                    'equipo' : equipo1,
                    'goles' : resultado1
                },
                'visitante': {
                    'equipo' : equipo2,
                    'goles' : resultado2
                },

            }

        resultArr[i-1] = jsonData

      }
      return (JSON.stringify({ 'items' : resultArr }))
};
